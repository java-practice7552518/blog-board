DROP TABLE IF EXISTS tbl_board;

CREATE TABLE tbl_board(
    boardId BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR (30) NOT NULL,
    content VARCHAR (30) NOT NULL,
    name VARCHAR (30) NOT NULL,
    createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modifiedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `read` INT DEFAULT NULL,
    memberId BIGINT
);

INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title1', 'content1', 'name1', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title2', 'content2', 'name2', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title3', 'content3', 'name3', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title4', 'content4', 'name4', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title5', 'content5', 'name5', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title6', 'content6', 'name6', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title7', 'content7', 'name7', 0);
INSERT INTO tbl_board(`title`, `content`, `name`, `read`) VALUES('title8', 'content8', 'name8', 0);
