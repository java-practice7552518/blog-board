package com.example.blog_board.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.blog_board.domain.Board;
import com.example.blog_board.mapper.BoardMapper;

@Service
public class BoardServiceImpl implements BoardService {

    @Autowired
    BoardMapper boardMapper;

    @Override
    public int boardCount() {
        return boardMapper.boardCount();
    }

    @Override
    public List<Board> boardList() {
        return boardMapper.boardList();
    }

    @Override
    public Board findById(Long boardId) {
        return boardMapper.findById(boardId);
    }

    @Override
    public Long add(Board board) {
        return boardMapper.save(board);
    }

    @Override
    public Long update(Board board) {
        return boardMapper.update(board);
    }

    @Override
    public Long increaseReadCount(Board board) {
        return boardMapper.increaseReadCount(board);
    }


    @Override
    public void deleteById(Long boardId) {
        boardMapper.delete(boardId);
    }
}
