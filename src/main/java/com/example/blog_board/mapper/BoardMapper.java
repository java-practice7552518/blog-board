package com.example.blog_board.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.blog_board.domain.Board;

@Mapper
public interface BoardMapper {

        @Select("SELECT count(BoardId) AS cbt FROM tbl_board")
        public int boardCount();

        @Select("SELECT * FROM tbl_board")
        public List<Board> boardList();

        @Select("SELECT * FROM tbl_board WHERE boardId=#{boardId}")
        public Board findById(Long boardId);

        @Insert("INSERT INTO" +
                "tbl_board (`title`, `content`, `name`, `read`)" +
                " VALUES (#{title}, #{content}, #{name}, #{read})")
        public Long save(Board board);

        @Update("UPDATE tbl_board" +
                " SET title = #{title}, content = #{content}, name = #{name}, modifiedDate = CURRENT_TIMESTAMP"+
                " WHERE boardId = #{boardId}")
        public Long update(Board board);

        @Update("UPDATE tbl_board" +
                " SET `read` = `read` + 1" +
                " WHERE boardId = #{boardId}")
        public Long increaseReadCount(Board board);

        @Delete("DELETE" +
                " FROM tbl_board" +
                " WHERE boardId = #{boardId}")
        public void delete(Long boardId);

}
