package com.example.blog_board.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class LoggingAop {
    /*
     * point Cut과 Around를 사용하여 Controller의 각 API가 실행하는 시간을 로깅 출력
     */
    @Pointcut("execution(* com.example.blog_board.controller.*.*(..))")
    public void controllerPointcut() {
    }

    @Around("controllerPointcut()")
    public Object processTimeLog(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object result = joinPoint.proceed();
        stopWatch.stop();

        long timeElapsedMs = stopWatch.getTotalTimeMillis();

        Signature signature = joinPoint.getSignature();
        String controllerName = signature.getDeclaringType().getName();
        String methodName = signature.getName();

        log.info("{}::{}, 실행시간 = {}ms", controllerName, String.format("%-20s", methodName), timeElapsedMs);

        return result;
    }

}
